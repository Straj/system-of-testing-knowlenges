var express = require('express');
var http = require('http');
var path = require('path');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var errorHandler = require('errorhandler');

var app = express();
var connection  = require('express-myconnection'); 
var mysql = require('mysql');

var routes = {
    common: require('./routes/common'),
    users: require('./routes/users')
}

app.set('port', process.env.PORT || 5000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride());

app.use(express.static(path.join(__dirname, 'public')));



if ('development' == app.get('env')) {
  app.use(errorHandler(console.log('error')));
}

app.use(   
    connection(mysql,{
        
        host: 'localhost',
        user: 'root',
        password : '',
        port : 3306, //port mysql
        database:'db'

    },'pool') //or single

);

/*pish.connect(function(err) {
  if ( !err ) {
    console.log("Connected to MySQL");
  } else if ( err ) {
    console.log(err);
  }
});*/

app.get('/', routes.common.index);
app.get('/users', routes.users.view);
app.get('/users/add', routes.users.add);
app.post('/users/add', routes.users.save);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));  
});
