-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `db` ;

-- -----------------------------------------------------
-- Table `db`.`Test`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db`.`Test` (
  `id_test` INT NOT NULL AUTO_INCREMENT,
  `testName` TEXT(100) NULL,
  `Testcol` VARCHAR(45) NULL,
  PRIMARY KEY (`id_test`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db`.`Question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db`.`Question` (
  `id_Question` INT NOT NULL AUTO_INCREMENT,
  `question` TEXT(250) NULL,
  `Test_id` INT NOT NULL,
  PRIMARY KEY (`id_Question`, `Test_id`),
  INDEX `fk_Question_Test1_idx` (`Test_id` ASC),
  CONSTRAINT `fk_Question_Test1`
    FOREIGN KEY (`Test_id`)
    REFERENCES `db`.`Test` (`id_test`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db`.`Answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db`.`Answer` (
  `id_Answer` INT NOT NULL AUTO_INCREMENT,
  `correct` TINYINT(1) NULL,
  `answer` TEXT(250) NULL,
  `Question_id_Question` INT NOT NULL,
  `Question_Test_id` INT NOT NULL,
  PRIMARY KEY (`id_Answer`),
  INDEX `fk_Answer_Question1_idx` (`Question_id_Question` ASC, `Question_Test_id` ASC),
  CONSTRAINT `fk_Answer_Question1`
    FOREIGN KEY (`Question_id_Question` , `Question_Test_id`)
    REFERENCES `db`.`Question` (`id_Question` , `Test_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db`.`Theme`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db`.`Theme` (
  `idTheme` INT NOT NULL AUTO_INCREMENT,
  `themeName` TEXT(100) NULL,
  `Test_id_test` INT NOT NULL,
  PRIMARY KEY (`idTheme`),
  INDEX `fk_Theme_Test1_idx` (`Test_id_test` ASC),
  CONSTRAINT `fk_Theme_Test1`
    FOREIGN KEY (`Test_id_test`)
    REFERENCES `db`.`Test` (`id_test`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db`.`User` (
  `idUser` INT NOT NULL,
  `Login` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idUser`))
ENGINE = InnoDB;


INSERT INTO `User` (`idUser`, `Login`) VALUES
(2, 'Nadya Eka'),
(3, 'Amali'),
(4, 'Angel');
-- -----------------------------------------------------
-- Table `db`.`User_has_Test`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db`.`User_has_Test` (
  `User_idUser` INT NOT NULL,
  `Test_id_test` INT NOT NULL,
  `score` INT NOT NULL,
  PRIMARY KEY (`Test_id_test`, `User_idUser`),
  INDEX `fk_User_has_Test_Test1_idx` (`Test_id_test` ASC),
  INDEX `fk_User_has_Test_User1_idx` (`User_idUser` ASC),
  CONSTRAINT `fk_User_has_Test_User1`
    FOREIGN KEY (`User_idUser`)
    REFERENCES `db`.`User` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Test_Test1`
    FOREIGN KEY (`Test_id_test`)
    REFERENCES `db`.`Test` (`id_test`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
