/*Save the user*/
exports.save = function(req,res){
    
    var input = JSON.parse(JSON.stringify(req.body));
    
    req.getConnection(function (err, connection) {
        
        var data = {            
            idUser  : input.idUser,
            Login : input.Login,  
        };
        
        var query = connection.query("INSERT INTO User set ? ",data, function(err, rows)
        {
  
          if (err)
              console.log("Error inserting : %s ",err );
         
          res.redirect('/users');
          
        });
        
       // console.log(query.sql); get raw query
    
    });
};



exports.add = function(req, res){
  res.render('add_customer',{page_title:"Add User - Node.js"});  
};

exports.view = function(req, res){

  req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM User',function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
     
            res.render('view_customers',{page_title:"Customers - Node.js",data:rows});
                
           
         });
         
         //console.log(query.sql);
    });
  
};